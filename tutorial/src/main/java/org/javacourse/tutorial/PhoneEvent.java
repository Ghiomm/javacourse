package org.javacourse.tutorial;

public class PhoneEvent {

	private String time;
	private String message;

	PhoneEvent(String time, String message){
		this.time = time;
		this.message = message;
	}
	
	
	public String getTime(){
		return time;
	}
	
	public String getMessage(){
		return message;
	}
	
}
