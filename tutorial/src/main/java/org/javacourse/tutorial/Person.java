package org.javacourse.tutorial;

/**
 * The root interface for objects representing a Person
 * 
 * @author ghiomm
 *
 */
public interface Person {
	
	/**
	 * @return The name of the person
	 */
	public String getName();
		
	/**
	 * @return The age of the person
	 */
	public int getAge();
	
	/**
	 * @return The address of the person
	 */
	public String getAddress();
	
	
	/**
	 * @return Tells this person to walk
	 */
	public void walk();
	
	/**
	 * @return <code>true<code> if the person is walking, <code>false<code> otherwise
	 */
	public boolean isWalking();
	
	/**
	 * @return Tells this person to stop walking
	 */
	public void stop();
		
	/**
	 * @return the distance this person has already walked
	 */
	public double distance();
	
}
