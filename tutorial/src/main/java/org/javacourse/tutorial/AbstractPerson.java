package org.javacourse.tutorial;



public abstract class AbstractPerson implements Person {


	//Fields
	//-----------------------------------------------------

	protected float pace = 0.005f;
	
	protected String name;		
	protected boolean isWalking = false;
	protected double distance = 0;

	protected Thread walkThread = null;

	/**
	 * The maximum distance that the person can walk before stopping of exhaustion
	 */
	protected double maxD = 20;


	//Constructor
	//--------------------------------------------------------
	public AbstractPerson(String name){
		this.name = name;
	}


	// METHODS
	//---------------------------------------------------------

	public String getName(){
		return name;
	}

	@Override
	public void walk(){

		if(!isWalking){
			Walk walk = new Walk(pace, 10);
			walkThread = new Thread(walk);
			walkThread.start();
			isWalking = true;
			System.out.println(name + " : is now walking");
		}
		else
			System.out.println(name + " : is already walking");

	}

	@Override
	public void stop() {
		if(isWalking){
			walkThread.interrupt();
			System.out.println(name + " has stopped walking");
		}
		else
			System.out.println(name + " was not walking");

		isWalking = false;
	}

	public boolean isWalking(){
		return isWalking;
	}

	public double distance(){
		System.out.println(name + " has walked " + distance + "km");
		return distance;
	}




	private class Walk implements Runnable{


		private final double pace;
		private final double maxD;

		private Walk(double p, double maxD){
			pace = p;
			this.maxD = maxD;
		}

		@Override
		public void run() {

			while(distance<maxD){

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					//Nothing to do
				}	
				distance += pace;				
			}


		}

	}


}
