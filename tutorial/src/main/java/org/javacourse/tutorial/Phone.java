package org.javacourse.tutorial;

import java.util.ArrayList;
import java.util.List;

public class Phone {

	List<PhoneListener> listeners = new ArrayList<>();
	
	
	public void addListener(PhoneListener l){
		listeners.add(l);
	}
	
	public void notifyListener(){
		
		PhoneEvent evt = new PhoneEvent(""+System.currentTimeMillis(), "Some event");
		
		for(int i = 0; i<listeners.size(); i++){
			PhoneListener l = listeners.get(i);
			l.eventReceived(evt);
		}
	}
	
	
}
