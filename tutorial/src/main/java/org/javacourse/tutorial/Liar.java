package org.javacourse.tutorial;

//Inheritance
public class Liar extends AbstractPerson{

	private int age;
	private String address;




	public Liar(String name, int age, String address) {
		super(name);	
		this.age = age;
		this.address = address;
	}

	
	
	
	@Override
	public String getName(){
		char[] seq = name.toCharArray();
		char[] reverted = new char[seq.length];
		for(int s=0, r=seq.length-1; s<seq.length; s++, r--)
			reverted[s] = seq[r];		
		return new String(reverted);
	}




	@Override
	public int getAge() {
		return age;
	}




	@Override
	public String getAddress() {
		return address;
	}




	
	
}
