package org.javacourse.tutorial;

import org.junit.Test;

public class ListenerTest {

	@Test
	public void testMethod(){
		
		Phone phone = new Phone();
		
		PhoneListener l = new ConcreteListener();
		
		phone.addListener(l);
		
		phone.notifyListener();
		
	}
	
}
