package org.javacourse.tutorial;

import org.junit.Test;

public class TestPerson {


	@Test
	public void testFlow(){

		//recap of arrays		
		int[] array = new int[20];

		int[][] other = 
			{
				{5,2,4,5,6},
				{2,5,8},
				{5,87,96,2}
			};


		//for loops
		for(int i = 0; i<other.length; i++){
			int sum = 0;
			for(int j = 0; j<other[i].length; j++){
				sum = sum + other[i][j];
			}	
			System.out.print("\n "+ sum);
		}




		//if else
		for(int i = 0; i<other.length; i++){
			int threshold = 10;
			for(int j = 0; j<other[i].length; j++){

				if(other[i][j]>threshold){
					System.out.println("Value is above threshold");
				}
				else
					System.out.println("Value is below threshold");

			}	

		}



		//switch
		int counter5 = 0, counter2 = 0;
		for(int i = 0; i<other.length; i++){			
			for(int j = 0; j<other[i].length; j++){

				switch(other[i][j]){
				case 5 : counter5++;
				break;
				case 2 : counter2++;
				break;
				default: //do nothing
					break;
				}

			}	

		}
		System.out.println("Number of 2 :"+counter2);
		System.out.println("Number of 5 :"+counter5);




		//break and labelled breaks


	}



	@Test
	public void testPerson() throws InterruptedException{

		Character filip = new Character("Filip", 31 , "Somewhere in Newington");	

		filip.setPace(0.5f);
		
		filip.walk();

		
		
		while(filip.isWalking()){
			Thread.sleep(500);
			if(filip.distance()>1)
				filip.stop();
		}

	}
	
	@Test
	public void testBolt(){
		
	}


}
